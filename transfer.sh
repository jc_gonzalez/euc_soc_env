#!/bin/bash
#------------------------------------------------------------------------------------
# transfer.sh
# Copy or move with secure copy from one place to another, but with a suffix before
# the transfer is finished to avoid event processing
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2015-2020 by Euclid SOC Team
#------------------------------------------------------------------------------------

if [ $# -lt 2 ]; then
    echo "usage:   $0  <filesrc>  ( <filetgt> | <dirtgt> )"
    echo ""
    echo "If the target is a directory, it must be ended with a slash '/'."
    exit 1
fi

# Get command line arguments
FROM="$1"
TO="$2"

# Resolve actual source and target files and addresses
FILE_TGT=${TO##*/}

if [ -z "$FILE_TGT" ]; then
    FILE_SRC=${FROM##*/}
    TGT="${TO}${FILE_SRC}"
else
    TGT="${TO}"
fi

TGT_PART="${TGT}.part"
TGT_ADDR=${TO%%:*}
FULL_FILE_TGT=${TGT#*:}

# First, do the actual copy
scp -qC ${FROM} ${TGT_PART} || \
    (echo "ERROR: Transfer $FROM => $TO failed."; exit 1)

# Second, rename target file
MV="mv"
if [ -n "$TGT_ADDR" -a \( "$TGT_ADDR" != "$TO" \) ]; then
    MV="ssh -C ${TGT_ADDR} mv"
fi
${MV} ${FULL_FILE_TGT}.part ${FULL_FILE_TGT} || \
    (echo "ERROR: Couldn't change target name ${FULL_FILE_TGT}.part => ${FULL_FILE_TGT}."; exit 1)

exit 0
