#!/bin/bash
#------------------------------------------------------------------------------------
# sis_controller.sh
# Launcher for SIS Controller tool
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2015-2020 by Euclid SOC Team
#------------------------------------------------------------------------------------
SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export PATH=${SCRIPTPATH}:${PATH}

# Path hanging from HOME to store the subsystem directories
BASE=${BASE_PATH:-${HOME}/SOC}

# Root for GIT repositories
GITBASE="https://jc_gonzalez@bitbucket.org/jc_gonzalez"

# Variable to allow LOCAL tests (LOCAL_TEST must be set to "yes")
SINGLE_MACHINE=${LOCAL_TEST:-no}

export XTERM_LOG_OPTS="-bg navyblue -fg lightgray -geometry 160x28 -title Logfiles"

for pytn in python3.7 python3.6 python3 ; do
    which $pytn 1>/dev/null 2>&1 && export PYTHON=$pytn
done
echo "PYTHON 3 is $PYTHON"

LOCALHOST_USER=$(cat localhost.user)
LOCALHOST_PWD=$(cat localhost.pwd)

MT=$(which multitail 2>/dev/null)
if [ -n "$MT" ]; then
    export SHOW_LOGS="xterm $XTERM_LOG_OPTS -e multitail -F ~/bin/multitail.conf -cS log4j"
else
    export SHOW_LOGS="tail -f"
fi

export TRANSFER="transfer.sh"

if [ "${SINGLE_MACHINE}" == "no" ]; then

    BASE=${BASE#${HOME}/}

    # ECSRV
    export ECSRVHOST="eucproc01.n1vs.lan"
    export ECSRVUSER="eucops"
    export ECSRVPWD="eu314clid"
    export ECSRVIN="/home/${ECSRVUSER}/${BASE}/ecsrv/in"

    # EAS
    export EASHOST="eucproc01.n1vs.lan"
    export EASUSER="eucops"
    export EASPWD="eu314clid"
    export EASIN="/home/${EASUSER}/${BASE}/eas/in"

    # ESS
    export ESSHOST="eucdev.n1data.lan"
    export ESSUSER="eucops"
    export ESSPWD="eu314clid"
    export ESSIN="/home/${ESSUSER}/${BASE}/ess/in"

    # HMS
    export HMSHOST="eucares7.n1data.lan"
    export HMSUSER="eucares"
    export HMSPWD="5EmRDrtU"
    export HMSIN="/home/${HMSUSER}/${BASE}/hms/in"

    export HMSHOSTint="eucares6.net3.lan"
    export HMSUSERint="ares"
    export HMSPWDint="5EmRDrtU"
    export HMSINint="/home/${HMSUSERint}/${BASE}/hms/in"

    # LE1
    export LE1HOST="eucproc03.n1vs.lan"
    export LE1USER="eucops"
    export LE1PWD="eu314clid"
    export LE1IN="/home/${LE1USER}/${BASE}/le1/in"

    # QLA
    export QLAHOST="eucproc03.n1vs.lan"
    export QLAUSER="eucops"
    export QLAPWD="eu314clid"
    export QLAIN="/home/${QLAUSER}/spf/data/inbox"

    # MOC
    export MOCHOST="eucproc03.n1vs.lan"
    export MOCUSER="eucops"
    export MOCPWD="eu314clid"
    export MOCIN="/home/${MOCUSER}/${BASE}/moc/in"

    # SCS
    export SCSHOST="eucflexplan.net3.lan"
    export SCSUSER="eucops"
    export SCSPWD="eu314clid"
    export SCSIN="/home/${SCSUSER}/${BASE}/scs/in"
    export SCSINICR="${SCSIN}/icr"
    export SCSINMIB="${SCSIN}/mib"
    export SCSINEVT="${SCSIN}/evt"
    export SCSINSSRFD="${SCSIN}/ssrfd"

    # SIS
    export SISHOST="eucsis.n1data.lan"
    export SISUSER="eucops"
    export SISPWD="eu314clid"
    export SISBASE="${HOME}/${BASE}/sis/SOCIF"

else

    # ECSRV
    export ECSRVHOST="localhost"
    export ECSRVUSER="${LOCALHOST_USER}"
    export ECSRVPWD="${LOCALHOST_PWD}"
    export ECSRVIN="${BASE}/eucproc01/ecsrv/in"

    # EAS
    export EASHOST="localhost"
    export EASUSER="${LOCALHOST_USER}"
    export EASPWD="${LOCALHOST_PWD}"
    export EASIN="${BASE}/eucproc01/eas/in"

    # ESS
    export ESSHOST="localhost"
    export ESSUSER="${LOCALHOST_USER}"
    export ESSPWD="${LOCALHOST_PWD}"
    export ESSIN="${BASE}/eucpedro/ess/in"

    # HMS
    export HMSHOST="localhost"
    export HMSUSER="${LOCALHOST_USER}"
    export HMSPWD="${LOCALHOST_PWD}"
    export HMSIN="${BASE}/eucares6/hms/in"

    # LE1
    export LE1HOST="localhost"
    export LE1USER="${LOCALHOST_USER}"
    export LE1PWD="${LOCALHOST_PWD}"
    export LE1IN="${BASE}/eucproc01/le1/in"

    # QLA
    export QLAHOST="localhost"
    export QLAUSER="${LOCALHOST_USER}"
    export QLAPWD="${LOCALHOST_PWD}"
    export QLAIN="${BASE}/eucproc01/qla/in"

    # MOC
    export MOCHOST="localhost"
    export MOCUSER="${LOCALHOST_USER}"
    export MOCPWD="${LOCALHOST_PWD}"
    export MOCIN="${BASE}/eucproc01/moc/in"

    # SCS
    export SCSHOST="localhost"
    export SCSUSER="${LOCALHOST_USER}"
    export SCSPWD="${LOCALHOST_PWD}"
    export SCSIN="${BASE}/eucflexplan/scs/in"
    export SCSINICR="${SCSIN}/icr"
    export SCSINMIB="${SCSIN}/mib"
    export SCSINEVT="${SCSIN}/evt"
    export SCSINSSRFD="${SCSIN}/ssrfd"

    # SIS
    export SISHOST="localhost"
    export SISUSER="${LOCALHOST_USER}"
    export SISPWD="${LOCALHOST_PWD}"
    export SISBASE="${BASE}/eucsis/sis/SOCIF"

fi

if [ -f "${SCRIPTPATH}/env.additional" ]; then
    source "${SCRIPTPATH}/env.additional"
fi
